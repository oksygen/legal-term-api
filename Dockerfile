
# Use a single-stage build for simplicity
FROM python:3.11-slim

# Set WORKDIR
WORKDIR /app

# Install Poetry
RUN pip install poetry
# RUN curl -sSL https://install.python-poetry.org | python3 -
RUN which poetry 
RUN poetry --version
# Install dependencies
COPY poetry.lock pyproject.toml /app/
# RUN poetry config virtualenvs.create false \
#     && poetry install --no-root --no-interaction --no-ansi
RUN poetry lock
RUN poetry install --no-root
# Copy project files
COPY . /app

# Expose port (optional, Uvicorn handles it by default)
EXPOSE 8000 

# Set Uvicorn command using environment variables for flexibility
ENV API_ENTRYPOINT="src.api:app"
CMD ["uvicorn", "src.api:app", "--host", "0.0.0.0", "--port", "8000"]



# # Use a multi-stage build for efficiency
# FROM python:3.11-slim AS build

# # Install git and other build dependencies
# RUN apt-get update && apt-get install -y git

# # Set WORKDIR
# WORKDIR /app

# # Install Poetry
# RUN pip install poetry

# # Copy only the dependency files
# COPY poetry.lock pyproject.toml /app/

# # Install dependencies
# RUN poetry config virtualenvs.create false \
#     && poetry install --no-root --no-interaction --no-ansi

# # Second stage to reduce image size
# FROM python:3.11-slim

# # Set WORKDIR
# WORKDIR /app

# # Copy installed dependencies from the previous stage
# COPY --from=build /usr/local/lib/python3.11/site-packages /usr/local/lib/python3.11/site-packages

# # Copy project files
# COPY . /app

# # Expose port (optional, Uvicorn handles it by default)
# EXPOSE 8000 

# # Set Uvicorn command using environment variables for flexibility
# ENV API_ENTRYPOINT="src.api:app"
# CMD ["uvicorn", "src.api:app", "--host", "0.0.0.0", "--port", "8000"]
